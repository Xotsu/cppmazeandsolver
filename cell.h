#ifndef CELL_H
#define CELL_H
namespace cellNamespace
{
	class Cell
	{
	public:
		int x;
		int y;
		int parent = -1;
		bool visited = false;
		char c = '#';
		Cell(int initx, int inity)
		{
			x = initx;
			y = inity;
		}

		void fillEmpty()
		{
			c = ' ';
		}
		void fillPath()
		{
			c = '.';
		}
		// FOCUS ON CHECKING AROUND THE CURRENT SQUARE NOT NEXT NEXT NEXT >>>
	};

}
#endif
