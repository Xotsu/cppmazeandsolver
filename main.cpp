// generate nxn grid of #
// implement wilsons maze generation
// implement a* pathfinding
// ~implement frame display of generation/solution
// example video demo https://www.youtube.com/watch?v=xqqGXfZpsmU
// g++ main.cpp -o run.o && ./run.o

#include <iostream>
#include <fstream>
#include <string>
#include <stdio.h>
#include "cell.h"

using namespace std;

// if a square is empty then the squares around it count as a wall
// those squares should not be selected as roots for generation

// return 0 = cant move (out of bounds/ found generating), 1 = can move, 2 = found existing maze
int canMoveUp(int *array, int n, int x, int y)
{
	// check for bounds
	if (n < ((2 * x)))
	{
		return 0;
	}

	// check for space
	if (0)
	{
		return 0;
	}
}
int canMoveRight(int *array, int n, int x, int y)
{
	// check for bounds
	if ((n + 1) % x == 0 && (n + 2) % x == 0)
	{
		return 0;
	}

	// check for space
}
int canMoveDown(int *array, int n, int x, int y)
{
	// check for bounds
	if (n > ((y * x) - (2 * x) - 1))
	{
		return 0;
	}

	// check for space
}
int canMoveLeft(int *array, int n, int x, int y)
{
	// check for bounds
	if (n % x == 0 && (n - 1) % x == 0)
	{
		return 0;
	}

	// check for space
}

void aStar()
{
	cout << "A* algorithm to be implemented \n";
}

void wilsons(int *array)
{
	cout << "Class method \n";
}

int main()
{
	// for rect have y=2x
	int x = 20;
	int y = 10;

	cout << "x: ";
	cin >> x;
	cout << "y: ";
	cin >> y;

	// boarders
	x += 1;
	y += 1;

	// initialise the state array 0=space, 1=#
	int *array = new int[x * y];
	for (int i = 0; i < x * y; i++)
	{
		array[i] = 1;
	}
	// cout << array[x * y - 1] << "\n";

	// do wilsons algorithm
	wilsons(array);

	// output the maze
	for (int i = 0; i < y; i++)
	{
		for (int j = 0; j < x; j++)
		{
			cout << "#";
		}
		cout << "\n";
	}

	//example 8*8 output
	cout << "\n########\n#      #\n###### #\n#    # #\n# ## # #\n#  # # #\n## #   #\n########\n";

	// do a* alogorithm
	aStar();
	// output the maze

	delete[] array;
	return 0;
}
